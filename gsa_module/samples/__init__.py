"""Package containing routines to generate design using various sampling schemes
"""
from . import cmdln_args
from . import test_sample
from . import hammersley
from . import lhs
from . import lhs_opt
from . import sobol
from . import srs


__author__ = "Damar Wicaksono"
