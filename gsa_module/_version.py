# -*- coding: utf-8 -*-
"""
    gsa_module._version
    *******************

    Module with version number unified across project, used in the module,
    setup.py and other command line interfaces
"""
__version__ = "0.7.1"
