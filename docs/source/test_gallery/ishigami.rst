
-----------------
Ishigami Function
-----------------

Ishigami function is a 3-dimensional function introduce by Ishigami and Homma
[1]_,

.. math::

    f(\underline x) = \sin x_1 + a \sin^2 x_2 + b x^4_3 \sin x_1

.. math::
    x_i \sim U[-\pi, \pi]; i = 1, 2, 3


Implementation in Python3
-------------------------

Morris Screening Results
------------------------

References
----------

.. [1] T. Homma and A. Saltelli, "Importance measures in global sensitivity
       analysis of nonlinear models," Reliability Engineering and System
       Safety, vol. 52, pp. 1-17, 1996.
.. [2] A. Saltelli et al., "Sensitivity Analysis in Practice," John Wiley
       & Sons: West Sussex, 2004, pp. 196
